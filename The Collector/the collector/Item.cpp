#include "Item.h"
#include <cstdlib>

Item::Item(std::vector<sf::Texture>& itemTextures, sf::Vector2u screenSize)
{
	int chosenIndex = rand() % itemTextures.size();//chooses a random mumber
	sprite.setTexture(itemTextures[chosenIndex]);//picks what texture the random number has selected
	pointsValue = chosenIndex * 100 + 100;//picks the amount of points the item is worth using the random number
	//chooses random x and y positions for items to spawn, reduced by the size of the texure to avoid spawning off screen
	int positionX = rand() % (screenSize.x - itemTextures[chosenIndex].getSize().x);
	int positionY = rand() % (screenSize.y - itemTextures[chosenIndex].getSize().y);
	//sets position with x and y positions
	sprite.setPosition(positionX, positionY);
}
