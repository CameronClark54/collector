#pragma once
#include <SFML/Graphics.hpp>
#include <vector>


class Player
{
public:
	//Constructor
	Player(sf::Texture& playerTexture, sf::Vector2u screenSize);

	//functions to call player code
	void Input();
	void Update(sf::Time frameTime);
	void Reset(sf::Vector2u screenSize);

	//Variables used by player class
	sf::Sprite sprite;
	sf::Vector2f velocity;
	float speed;

};


