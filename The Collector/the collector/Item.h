#pragma once
#include <SFML/Graphics.hpp>
class Item
{
	public://access level

	//constructor
	Item(std::vector<sf::Texture> & itemTextures, sf::Vector2u screenSize);

	//variables
	sf::Sprite sprite;
	int pointsValue;
};
